<?php

function add_message($state, $message) {
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }

    if (!isset($_SESSION["flash"])) {
        $_SESSION["flash"] = array();
    }

    array_push($_SESSION["flash"], ["message" => $message, "state" => $state]);
}

function get_message() {
    if (session_status() == PHP_SESSION_NONE) {
        session_start();
    }

    $return_value = isset($_SESSION["flash"]) ? $_SESSION["flash"] : array();
    $_SESSION["flash"] = array();
    return $return_value;
}