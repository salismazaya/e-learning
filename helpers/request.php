<?php

require_once __DIR__ . "/../models/user.php";

class RequestUser
{
    public $id;
    public $name;
    public $username;
    public $type;
    public $profilePicture;

    public function __construct($id, $name, $username, $type, $profilePicture) {
        $this->id = $id;
        $this->name = $name;
        $this->username = $username;
        $this->type = $type;
        $this->profilePicture = $profilePicture;
    }
}


class Request
{

    static public function getUser() : RequestUser | null {
        if (!isset($_SESSION["user_id"])) {
            return null;
        }

        $users = User::fetch("id = ? LIMIT 1", [$_SESSION["user_id"]]);

        if (empty($users)) {
            return null;
        }

        $user = $users[0];

        return new RequestUser($user->id, $user->name, $user->username, $user->type, isset($user->profilePicture) ? $user->profilePicture : null);
    }
}