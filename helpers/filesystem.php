<?php

use Aws\S3\S3Client;


function get_filesystem()
{

    $client = $client = S3Client::factory([
        'region' => 'test',
        'version' => 'latest',
        'endpoint' => 'https://is3.cloudhost.id/siraga',
        'credentials' => [
            'key' => "343E752RHZSHKDUK9T38",
            'secret' => "uQzKxpBGfpTSLL0nqk77V5dg8mkfGVFCJA5sxhfg"
        ]
    ]);

    $adapter = new League\Flysystem\AwsS3V3\AwsS3V3Adapter(
        $client,
        'siraga',
        'e-learning',
        new League\Flysystem\AwsS3V3\PortableVisibilityConverter(
            League\Flysystem\Visibility::PUBLIC
        )
    );

    $filesystem = new League\Flysystem\Filesystem($adapter);
    return $filesystem;
}
