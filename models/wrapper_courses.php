<?php

require_once __DIR__ . '/base/base.php';

class WrapperCourse extends Model {
    static public function getTableName() {
        return 'wrapper_courses';
    }

    static public function getFields() {
        return [ 'id' => 'i', 'wrapper_name' => 's', 'mentor' => 'i', 'image' => 's'];
    }
}