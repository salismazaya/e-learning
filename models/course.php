<?php

require_once __DIR__ . '/base/base.php';

class Course extends Model
{
    static public function getTableName()
    {
        return 'courses';
    }

    static public function getFields()
    {
        return ['id' => 'i', 'title' => 's', 'video_id' => 's', 'description' => 's', 'wrapper' => 'i'];
    }
}