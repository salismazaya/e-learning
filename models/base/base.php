<?php

abstract class Model
{
    static public function getConnection()
    {
        return new mysqli('database', 'root', 'root', 'e-learning');
    }

    abstract static public function getTableName();
    abstract static public function getFields();

    static public function fetch(string $postfixSql = "id IS NOT NULL", array $value = array(), string $beforePostfixSql = "", $fieldsSql = "*")
    {
        $conn = static::getConnection();
        $tableName = static::getTableName();
        $sql = "SELECT $fieldsSql FROM $tableName $beforePostfixSql WHERE $postfixSql";
        for ($i = 0; $i < count($value); $i++) {
            $sql = str_replace("?", mysqli_real_escape_string($conn, $value[$i]), $sql);
        }
        $exec = $conn->query($sql);
        $result = array();

        while ($row = $exec->fetch_object()) {
            array_push($result, $row);
        }

        return $result;
    }

    static public function insert(array $data)
    {
        $conn = static::getConnection();
        $tableName = static::getTableName();
        $tableFields = join(", ", array_keys($data));
        $tableValues = array();
        $fields = static::getFields();

        $tableTypeData = "";
        foreach ($data as $key => $value) {
            $tableTypeData .= $fields[$key];
        }

        for ($i = 0; $i < count($data); $i++) {
            array_push($tableValues, '?');
        }
        $tableValues = join(", ", $tableValues);
        $sql = "INSERT INTO $tableName($tableFields) VALUES($tableValues)";
        $pre = $conn->prepare($sql);
        $values = array_values($data);
        $pre->bind_param($tableTypeData, ...$values);
        if (!$pre->execute()) {
            throw new Exception($pre->error);
        }

        return true;
    }

    static public function delete(string $postfixSql)
    {
        $conn = static::getConnection();
        $tableName = static::getTableName();
        $sql = "DELETE FROM $tableName WHERE $postfixSql";
        $conn->query($sql);
        return true;
    }

    static public function update(array $data, $postfixSql)
    {
        $conn = static::getConnection();
        $tableName = static::getTableName();
        $setSql = array();

        foreach ($data as $key => $value) {
            array_push($setSql, "$key = ?");
        }

        $setSql = join(", ", $setSql);
        $fields = static::getFields();
        $tableTypeData = "";
        foreach ($data as $key => $value) {
            $tableTypeData .= $fields[$key];
        }
        
        $sql = "UPDATE $tableName SET $setSql WHERE $postfixSql";
        $pre = $conn->prepare($sql);
        $values = array_values($data);
        $pre->bind_param($tableTypeData, ...$values);
        if (!$pre->execute()) {
            throw new Exception($pre->error);
        }

        return true;
    }
}