<?php

require_once __DIR__ . '/base/base.php';

class User extends Model
{
    static public function getTableName()
    {
        return 'users';
    }


    static public function getFields()
    {
        return ['id' => 'i', 'name' => 's', 'username' => 's', 'type' => 's', 'password' => 's', 'profile_picture' => 's'];
    }
}