<?php

require __DIR__ . "/../vendor/autoload.php";
require __DIR__ . '/../controllers/profile.php';

$controller = new ProfileController();
echo $controller->handle();