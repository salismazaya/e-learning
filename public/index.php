<?php

require __DIR__ . "/../vendor/autoload.php";
require '../controllers/index.php';

$controller = new IndexController();
echo $controller->handle();