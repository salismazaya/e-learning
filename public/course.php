<?php

require __DIR__ . "/../vendor/autoload.php";
require __DIR__ . '/../controllers/course.php';

$controller = new CourseController();
echo $controller->handle();