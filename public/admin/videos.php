<?php

require '../../controllers/admin/videos.php';
require __DIR__ . "/../../vendor/autoload.php";

$controller = new AdminListVideosView();
echo $controller->handle();