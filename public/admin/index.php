<?php

require '../../controllers/admin/courses.php';
require __DIR__ . "/../../vendor/autoload.php";

$controller = new AdminCoursesListController();
echo $controller->handle();