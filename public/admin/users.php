<?php

require '../../controllers/admin/users.php';
require __DIR__ . "/../../vendor/autoload.php";


$controller = new UsersAdminBaseController();
echo $controller->handle();