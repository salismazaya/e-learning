<?php

require __DIR__ . "/../vendor/autoload.php";
require __DIR__ . '/../controllers/courses.php';

$controller = new CoursesController();
echo $controller->handle();