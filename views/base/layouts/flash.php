<?php

require_once __DIR__ . "/../../../helpers/flash.php";

foreach (get_message() as $key => $value) {
    ?>
    <div class="tw-p-3 tw-m-3 tw-text-xl tw-drop-shadow-lg tw-border tw-rounded tw-text-black <?php if ($value['state'] == "error") {
        echo 'tw-bg-red-100 tw-border-red-500';
    } elseif ($value['state'] == "success") {
        echo "tw-bg-green-100 tw-border-green-500";
    } ?>">
        <p><?= htmlspecialchars($value['message']) ?></p>
    </div>
    <?php
}