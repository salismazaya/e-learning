<?php

require_once __DIR__ . "/../../../../helpers/request.php";

function sidebar()
{
    $user = Request::getUser();
    ?>
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="/admin" class="brand-link">
            <img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
                style="opacity: .8">
            <span class="brand-text font-weight-light">Free Learning</span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="image">
                    <img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
                </div>
                <div class="info">
                    <a href="/profile.php" class="d-block"><?= $user->name ?></a>
                </div>
            </div>

            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    <li class="nav-header">Shortcut</li>
                    <li class="nav-item">
                        <a href="/" class="nav-link <?php if (str_contains($_SERVER['REQUEST_URI'], "users.php"))
                            echo "active" ?>">
                                <i class="nav-icon fa fa-home"></i>
                                <p>
                                    Home
                                </p>
                            </a>
                        </li>
                </nav>

                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <li class="nav-header">Daftar Menu</li>
                    <?php if ($user->type === 'superuser') { ?>
                        <li class="nav-item">
                            <a href="users.php" class="nav-link <?php if (str_contains($_SERVER['REQUEST_URI'], "users.php"))
                                echo "active" ?>">
                                    <i class="nav-icon fa fa-user"></i>
                                    <p>
                                        Users
                                    </p>
                                </a>
                            </li>
                    <?php } ?>
                    <li class="nav-item">
                        <a href="/admin" class="nav-link <?php if (str_ends_with($_SERVER['REQUEST_URI'], "/admin") || str_ends_with($_SERVER['REQUEST_URI'], "/admin/"))
                            echo "active" ?>">
                                <i class="nav-icon far fa-image"></i>
                                <p>
                                    Courses
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="videos.php" class="nav-link <?php if (str_contains($_SERVER['REQUEST_URI'], "videos.php"))
                            echo "active" ?>">
                                <i class="nav-icon fa fa-play"></i>
                                <p>
                                    Videos
                                </p>
                            </a>
                        </li>
                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>
    <?php
}