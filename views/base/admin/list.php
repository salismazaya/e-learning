<?php

require __DIR__ . "/base_site.php";
require __DIR__ . "/../../../helpers/filesystem.php";

abstract class AdminListView extends AdminBaseSiteView
{
    public $data;
    public $value = [];
    public function __construct($data)
    {
        $this->data = $data;
    }

    abstract static public function get_fields();
    abstract public function get_title();

    public function set_value($value)
    {
        $this->value = $value;
    }

    public function script()
    {
        ?>
        <script>
            let data_to_delete = [];
            $('.checkbox-delete').on('change', (data) => {
                if (data_to_delete.includes(data.target.id)) {
                    data_to_delete = data_to_delete.filter(item => item !== data.target.id);
                } else {
                    data_to_delete.push(data.target.id);
                }

                if (data_to_delete.length >= 1) {
                    $('#delete-button').removeClass("tw-hidden");
                } else {
                    $('#delete-button').addClass("tw-hidden");
                }
            })

            $('#delete-button').on('click', () => {
                Swal.fire({
                    icon: 'warning',
                    text: "Are you sure?",
                    showCancelButton: true
                }).then(x => {
                    if (x.isConfirmed) {
                        $.post('?action=delete&ids=' + data_to_delete.join(','), {}, () => {
                            window.location.reload();
                        });
                    }
                })
            });
        </script>
        <?php
    }

    public function content()
    {
        if (isset($_GET['id']) || isset($_GET['action']) && $_GET['action'] == 'add') {
            return $this->content_edit();
        }
        $fields = $this->get_fields();
        ?>
        <div class="content-wrapper">
            <div class="tw-flex">
                <a href="?action=add" class="btn btn-primary tw-mx-4 tw-mt-5">+ Add</a>
                <!-- <form action="?action=delete" method="post"> -->
                    <button class="btn btn-danger tw-mx-4 tw-mt-5 tw-hidden" id="delete-button">- Delete</button>
                <!-- </form> -->

            </div>
            <div class="row">
                <form class="tw-flex tw-w-full tw-items-center">
                    <div class="input-group mb-3 p-3 tw-w-full">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-search"></i></span>
                        </div>
                        <input name="search" type="text" class="form-control" placeholder="Search" value="<?php if (isset($_GET['search']))
                            echo $_GET['search'] ?>">
                        </div>
                        <!-- <button class="tw-h-10 tw-w-2/12 btn btn-primary">Search</button> -->
                    </form>
                    <div class="col-12 table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>ID</th>
                                <?php foreach ($fields as $key => $value) { ?>
                                    <th class="tw-uppercase"><?= $key ?></th>
                                <?php } ?>

                            </tr>
                        </thead>
                        <tbody>
                            <?php for ($i = 0; $i < count($this->data); $i++) { ?>
                                <tr>
                                    <td>
                                        <input type="checkbox" name="delete" class="checkbox-delete"
                                            id="<?= $this->data[$i]->id ?>">
                                    </td>
                                    <td>
                                        <a href="?id=<?= $this->data[$i]->id ?>"><?= $this->data[$i]->id ?></a>
                                    </td>
                                    <?php foreach ($fields as $key => $value) {
                                        if (is_countable($value) && count($value) == 4 && $value[3] === 0) {
                                            $value_id = $this->data[$i]->{$key};
                                            $fk_data = $value[0]::fetch("id = $value_id")[0];
                                            $td_value = $fk_data->{$value[2]} != null ? htmlspecialchars($fk_data->{$value[2]}): '-' ;
                                        } else {
                                            $td_value = $this->data[$i]->{$key} != null ? htmlspecialchars($this->data[$i]->{$key}) : '-';

                                        }
                                        ?>
                                        <td><?= $td_value ?></td>
                                    <?php } ?>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.col -->
            </div>
        </div>
        <?php
    }

    public function content_edit()
    {
        $fields = $this->get_fields();
        ?>
        <div class="content-wrapper">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title"><?= $this->get_title() ?></h3>
                </div>
                <form method="post" enctype="multipart/form-data">
                    <? require __DIR__ . "/../layouts/flash.php" ?>
                    <div class="card-body">
                        <?php foreach ($fields as $key => $value) { ?>
                            <?php if ($value == 'str') { ?>
                                <div class="form-group">
                                    <label for="<?= $key ?>" class="tw-uppercase"><?= $key ?></label>
                                    <input type="text" required class="form-control" id="<?= $key ?>" name="<?= $key ?>"
                                        placeholder="Enter <?= $key ?>" value="<?php if (isset($this->value[$key]))
                                              echo $this->value[$key] ?>">
                                    </div>
                            <?php } elseif (is_array($value) && count($value) == 4 && $value[3] === 0) {
                                $fk_data = $value[0]::fetch($value[1]);
                                // }
                                ?>
                                <div class="form-group">
                                    <label class="tw-uppercase"><?= $key ?></label>
                                    <select class="form-control" required name="<?= $key ?>">
                                        <option value="">---</option>
                                        <?php foreach ($fk_data as $key2 => $value2) { ?>
                                            <option <?php if (isset($this->value[$key]) && $this->value[$key] == $value2->id)
                                                echo 'selected' ?> value="<?= $value2->id ?>" class="tw-capitalize"><?= $value2->{$value[2]} ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            <?php } elseif (is_array($value)) { ?>
                                <div class="form-group">
                                    <label class="tw-uppercase"><?= $key ?></label>
                                    <select class="form-control" required name="<?= $key ?>">
                                        <option value="">---</option>
                                        <?php foreach ($value as $key2 => $value2) { ?>
                                            <option <?php if (isset($this->value[$key]) && $this->value[$key] == $value2)
                                                echo 'selected' ?> value="<?= $value2 ?>" class="tw-capitalize"><?= $value2 ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            <?php } elseif ($value == "video") { ?>
                                <div class="form-group">
                                    <label for="<?= $key ?>" class="tw-uppercase"><?= $key ?></label>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" name="<?= $key ?>" id="<?= $key ?>">
                                            <label class="custom-file-label" for="<?= $key ?>">Choose file</label>
                                        </div>
                                    </div>
                                </div>
                                <?php if (isset($this->value[$key])) { ?>
                                    <video class="tw-w-full lg:tw-w-1/2" controls
                                        src="<?= get_filesystem()->temporaryUrl($this->value[$key], (new DateTime())->modify('+60 minutes')) ?>"></video>
                                <?php } ?>
                            <?php } elseif ($value == "image") { ?>
                                <div class="form-group">
                                    <label for="<?= $key ?>" class="tw-uppercase"><?= $key ?></label>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" name="<?= $key ?>" id="<?= $key ?>">
                                            <label class="custom-file-label" for="<?= $key ?>">Choose file</label>
                                        </div>
                                    </div>
                                </div>
                                <?php if (isset($this->value[$key])) { ?>
                                    <img class="tw-w-full lg:tw-w-1/2"
                                        src="<?= get_filesystem()->temporaryUrl($this->value[$key], (new DateTime())->modify('+5 minutes')) ?>"></img>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>

                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                        <button class="btn btn-primary">Edit</button>
                    </div>
                </form>
            </div>
        </div>
        <?php
    }
}
?>