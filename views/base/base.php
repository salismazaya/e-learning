<?php

require_once 'layouts/head.php';

abstract class BaseView
{
    public function head()
    {
        return head();
    }

    abstract public function body();

    public function display()
    {
        ?>

        <!DOCTYPE html>
        <html lang="en">

        <head>
            <?= $this->head() ?>
        </head>

        <body>
            <?= $this->body() ?>
            <?= $this->script() ?>
        </body>

        </html>

        <?php
    }

    public function script() {
        return '';
    }
}