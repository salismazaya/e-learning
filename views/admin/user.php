<?php

require_once __DIR__ . "/../base/admin/base_site.php";

class AdminUserBaseView extends AdminBaseSiteView
{

    public $title = "User";

    public function getUsernameValue()
    {
        return '';
    }

    public function getNameValue()
    {
        return '';
    }

    public function getTypeValue()
    {
        return '';
    }

    public function content()
    {
        $self = Request::getUser();
        ?>

        <div class="content-wrapper">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title"><?= $this->title ?></h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form method="post">
                    <div class="card-body">
                        <div class="form-group">
                            <label for="username">Username</label>
                            <input type="text" required class="form-control" id="username" name="username"
                                placeholder="Enter username" value="<?= $this->getUsernameValue() ?>">
                        </div>
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" required class="form-control" name="name" id="name" placeholder="Enter name"
                                value="<?= $this->getNameValue() ?>">
                        </div>
                        <?php if ($self->type == "superuser") { ?>
                            <div class="form-group">
                                <label>Type</label>
                                <select class="form-control" required name="type">
                                    <option value="">---</option>
                                    <option <?php if ($this->getTypeValue() == 'user')
                                        echo 'selected' ?> value="user">User</option>
                                        <option <?php if ($this->getTypeValue() == 'mentor')
                                        echo 'selected' ?> value="mentor">Mentor
                                        </option>
                                        <option <?php if ($this->getTypeValue() == 'superuser')
                                        echo 'selected' ?> value="superuser">
                                            Superuser</option>
                                    </select>
                                </div>
                        <?php } ?>
                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                        <button class="btn btn-primary">Edit</button>
                    </div>
                </form>
            </div>
        </div>
        <?php
    }
}

class AdminUserEditView extends AdminUserBaseView
{
    public $user;
    public $title = "Edit User";

    public function __construct($user)
    {
        $this->user = $user;
    }

    public function getUsernameValue()
    {
        return $this->user->username;
    }

    public function getNameValue()
    {
        return $this->user->name;
    }

    public function getTypeValue()
    {
        return $this->user->type;
    }
}