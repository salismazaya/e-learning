<?php

require_once __DIR__ . "/../base/admin/list.php";
require_once __DIR__ . "/../../models/user.php";


class AdminCoursesView extends AdminListView
{
    public static function get_fields()
    {
        $user_id = Request::getUser()->id;
        $where_sql = "id = $user_id";
        if (Request::getUser()->type == "superuser") {
            $where_sql = "id IS NOT NULL";
        }
        return [
            'wrapper_name' => 'str',
            'image' => 'image',
            'mentor' => [User::class, $where_sql, "name", 0]
        ];
    }

    public function get_title() {
        return 'Course';
    }
}