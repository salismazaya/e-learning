<?php

require_once __DIR__ . "/../base/admin/list.php";

class AdminUsersView extends AdminListView
{
    public static function get_fields()
    {
        
        return [
            'name' => 'str',
            'username' => 'str',
            'type' => ['user', 'superuser', 'mentor']
        ];
    }

    public function get_title() {
        return 'User';
    }
}