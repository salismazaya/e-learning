<?php

require_once __DIR__ . "/../base/admin/list.php";
require_once __DIR__ . "/../../models/wrapper_courses.php";

class AdminVideosView extends AdminListView
{
    public static function get_fields()
    {
        $user_id = Request::getUser()->id;
        $where_sql = "mentor = $user_id";
        if (Request::getUser()->type == "superuser") {
            $where_sql = "id IS NOT NULL";
        }

        return ['title' => 'str', 'video_id' => 'video', 'description' => 'str', 'wrapper' => [WrapperCourse::class, $where_sql, "wrapper_name", 0]];
    }

    public function get_title()
    {
        return "Video";
    }

    public function get_search_fields()
    {
        return ['title'];
    }
}