<?php

require 'own/base_site.php';


class LoginView extends BaseSiteView
{
    public $errorMessage;
    
    public function setErrorMessage($message) {
        $this->errorMessage = $message;
    }
    public function content()
    {
        ?>
        <form method="post">
            <div class="container tw-my-40">
                <div class="row justify-content-center">
                    <div class="col-md-6">
                        <div class="card mt-5">
                            <div class="card-body">
                                <?php if (isset($this->errorMessage)) { ?>
                                    <p class="tw-bg-red-100 tw-border-red-500 tw-rounded tw-text-black p-3">
                                        <?= $this->errorMessage ?>
                                    </p>
                                <?php } ?>
                                <h3 class="card-title text-center">Login</h3>
                                <form>
                                    <div class="form-group">
                                        <label for="username">Username</label>
                                        <input type="username" name="username" required class="form-control" id="username"
                                            aria-describedby="usernameHelp" placeholder="Enter username">
                                        <small id="usernameHelp" class="form-text text-muted">We'll never share your username with
                                            anyone
                                            else.</small>
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="password" class="form-control" name="password" required id="password"
                                            placeholder="Password">
                                    </div>
                                    <!-- <div class="form-group form-check">
                                    <input type="checkbox" class="form-check-input" id="rememberMe">
                                    <label class="form-check-label" for="rememberMe">Remember me</label>
                                </div> -->
                                    <button type="submit" class="btn btn-primary btn-block mt-1">Login</button>
                                </form>
                                <!-- <div class="text-center mt-3">
                                <a href="#">Forgot your password?</a>
                            </div> -->
                                <div class="text-center mt-2">
                                    <span>Don't have an account? <a href="/register.php">Sign up</a></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <?php
    }
}