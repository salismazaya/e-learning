<?php

require_once __DIR__ . '/own/base_site.php';
require_once __DIR__ . '/../helpers/filesystem.php';
require_once __DIR__ . '/../helpers/request.php';

class ProfileView extends BaseSiteView
{
    public $user;

    function __construct($user)
    {
        $this->user = $user;
    }

    public function script()
    {
        ?>
        <script>
            function saveProfile() {
                var name = document.getElementById('profileNameInput').value;
                var username = document.getElementById('profileUsernameInput').value;

                document.getElementById('profileName').innerText = name;
                document.getElementById('profileUsername').innerText = username;

                // Close the modal
                $('#editProfileModal').modal('hide');
            }
        </script>
        <?php
    }

    public function content()
    {
        ?>

        <div class="container mt-5 tw-min-h-screen">
            <div class="row">
                <div class="col-md-4 offset-md-4">
                    <div class="card">
                        <div class="card-body text-center">
                            <img src="/img/team-1.jpg" class="rounded-circle img-fluid" alt="Profile Picture" width="150"
                                height="150">
                            <h5 class="card-title mt-3" id="profileName"><?= htmlspecialchars($this->user->name) ?></h5>
                            <p class="card-text" id="profileUsername">@<?= htmlspecialchars($this->user->username) ?></p>
                            <a href="/logout.php">Logout</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php
    }
}