<?php

require_once __DIR__ . '/own/base_site.php';
require_once __DIR__ . '/../helpers/filesystem.php';

class CoursesView extends BaseSiteView
{
    public $courses;

    function __construct($courses)
    {
        $this->courses = $courses;
    }

    public function content()
    {
        ?>
        <div class="container mt-5 tw-min-h-screen">
            <div class="tw-my-16 text-center wow fadeInUp" data-wow-delay="0.1s"
                style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;">
                <h6 class="section-title bg-white text-center text-primary px-3">Courses</h6>
            </div>
            <div class="row g-4 justify-content-center">
                <?php foreach ($this->courses as $key => $value) { ?>
                    <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.1s"
                        style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;">
                        <div class="course-item tw-w-96 tw-overflow-hidden tw-rounded-xl tw-bg-gray-50">
                            <div class="position-relative overflow-hidden">
                                <?php if ($value->image !== null) { ?>
                                    <img class="img-fluid tw-h-56 tw-w-full"
                                        src="<?= get_filesystem()->temporaryUrl($value->image, (new DateTime())->modify("+5 minutes")) ?>"
                                        alt="">
                                <?php } else { ?>
                                    <img class="img-fluid tw-h-56 tw-w-full"
                                        src="img/course-1.jpg"
                                        alt="">
                                <?php } ?>
                                <div class="w-100 d-flex justify-content-center position-absolute bottom-0 start-0 mb-4">
                                    <a href="course.php?wrapper=<?= $value->id ?>" class="flex-shrink-0 btn btn-sm btn-primary px-3 tw-text-lg"
                                        style="border-radius: 30px 30px 30px 30px;">Join Now</a>
                                </div>
                            </div>
                            <div class="text-center p-4 pb-0">
                                <h3 class="mb-0">Free</h3>
                                <div class="mb-3">
                                    <small class="fa fa-star text-primary"></small>
                                    <small class="fa fa-star text-primary"></small>
                                    <small class="fa fa-star text-primary"></small>
                                    <small class="fa fa-star text-primary"></small>
                                    <small class="fa fa-star text-primary"></small>
                                </div>
                                <h5 class="mb-4"><?= htmlspecialchars($value->wrapper_name) ?></h5>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
        <?php
    }
}