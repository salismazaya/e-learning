<?php

require_once __DIR__ . '/own/base_site.php';
require_once __DIR__ . '/../helpers/filesystem.php';
require_once __DIR__ . '/../helpers/request.php';

class CourseView extends BaseSiteView
{
    public $course;
    public $video;
    public $videos;

    function __construct($course, $video, $videos)
    {
        $this->course = $course;
        $this->video = $video;
        $this->videos = $videos;
    }

    public function content()
    {
        $expired = (new DateTime())->modify("+60 minutes");
        ?>

        <style>
        </style>

        <div class="container mt-5 tw-min-h-screen">
            <div class="row">
                <div class="<?= count($this->videos) >= 2 ? "col-md-8" : "col-md-12" ?>">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title tw-uppercase"><?= $this->course->wrapper_name ?></h5>
                            <p class="card-title tw-uppercase"><?= $this->video->title ?></p>
                            <div class="video-container">
                                <?php if (Request::getUser() !== null) { ?>
                                    <video class="tw-w-full tw-h-full" controls
                                        src="<?= get_filesystem()->temporaryUrl($this->video->video_id, $expired) ?>"></video>
                                <?php } else { ?>
                                    <div class="tw-w-full tw-h-full tw-text-red-500">
                                        <p>Login to access video</p>
                                    </div>
                                <?php } ?>
                            </div>
                            <p class="card-text mt-3" id="videoDescription">
                                <?php if ($this->video->description != null)
                                    echo "Description : " . htmlspecialchars($this->video->description) ?>
                                </p>
                            </div>
                        </div>
                    </div>
                <?php if (count($this->videos) >= 2) { ?>
                    <div class="col-md-4">
                        <p>Next Video</p>
                        <div class="list-group" id="playlist">
                            <?php foreach ($this->videos as $key => $value) { ?>
                                <?php if ($this->video->id != $value->id) { ?>
                                    <a href="?id=<?= $value->id ?>" class="list-group-item list-group-item-action">
                                        <div class="d-flex w-100 justify-content-between">
                                            <h5 class="mb-1"><?= htmlspecialchars($value->title) ?></h5>
                                        </div>
                                        <?php if (isset($this->course->image)) { ?>
                                            <img src="<?= get_filesystem()->temporaryUrl($this->course->image, $expired) ?>" class="tw-w-full tw-h-44"
                                                alt="Thumbnail Video">
                                        <?php } ?>
                                    </a>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>

        <?php
    }
}