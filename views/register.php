<?php

require 'own/base_site.php';

class RegisterView extends BaseSiteView
{
    public function content()
    {
        ?>
        <form method="post">
            <div class="row justify-content-center tw-my-40">
                <div class="col-md-6">
                    <div class="card mt-5">
                        <div class="card-body">
                            <?php if (isset($this->errorMessage)) { ?>
                                <p class="tw-bg-red-100 tw-border-red-500 tw-rounded tw-text-black p-3">
                                    <?= $this->errorMessage ?>
                                </p>
                            <?php } ?>
                            <h3 class="card-title text-center">Register</h3>
                            <form>
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" class="form-control" id="name" name="name" required
                                        placeholder="Enter your name">
                                </div>
                                <div class="form-group">
                                    <label for="username">Username</label>
                                    <input type="text" class="form-control" id="username" name="username" required
                                        placeholder="Enter your username">
                                </div>
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input type="password" class="form-control" id="password" name="password" required
                                        placeholder="Enter your password">
                                </div>
                                <div class="form-group">
                                    <label for="confirmPassword">Confirm Password</label>
                                    <input type="password" class="form-control" name="confirmPassword" required
                                        id="confirmPassword" placeholder="Confirm your password">
                                </div>
                                <button type="submit" class="btn btn-primary btn-block">Register</button>
                            </form>
                            <div class="text-center mt-3">
                                <span>Already have an account? <a href="/login.php">Login</a></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <?php
    }

    public function setErrorMessage($message)
    {
        $this->errorMessage = $message;
    }
}