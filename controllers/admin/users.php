<?php

require_once __DIR__ . '/../base/admin/list.php';
require_once __DIR__ . '/../../views/admin/videos.php';
require_once __DIR__ . '/../../models/user.php';

class UsersAdminBaseController extends AdminListBaseController
{
    public function handle()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        $user = Request::getUser();
        if ($user == null || $user->type != 'superuser') {
            return "Forbidden";
        }
        return parent::handle();
    }

    public function get_view()
    {
        return AdminUsersView::class;
    }

    public function get_data()
    {
        $postfix_sql = "id IS NOT NULL";
        $search_fields = $this->get_search_fields();
        if (isset($_GET["search"])) {
            $search = mysqli_real_escape_string(Model::getConnection(), $_GET["search"]);
            $value = "'%$search%'";
            $keren = array();
            // array_push()
            foreach ($search_fields as $field) {
                array_push($keren, "$field LIKE $value");
            }

            $postfix_sql = join(" OR ", $keren);
        }
        return User::fetch($postfix_sql);
    }

    public function get_model()
    {
        return User::class;
    }

    public function get_search_fields()
    {
        return ['name', 'username', 'type'];
    }
}