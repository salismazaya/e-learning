<?php

require_once __DIR__ . '/../base/admin/base.php';
require_once __DIR__ . '/../../views/admin/user.php';
require_once __DIR__ . '/../../helpers/request.php';
require_once __DIR__ . '/../../models/user.php';

class UserEditAdminBaseController extends AdminBaseController {
    public function get() {
        // $user = Request::getUser();
        $id = $_GET['id'];
        $user = User::fetch("id = $id")[0];
        $view = new AdminUserEditView($user);
        return $view->display();
    }

    public function handle() {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        $user = Request::getUser();
        if (($user == null || $user->type != 'superuser') && !(isset($_GET['id']) && $_GET['id'] == $user->id)) {
            return "Forbidden";
        }
        return parent::handle();
    }
    
    public function post() {
        $id = $_GET['id'];
        User::update($_POST, "id = $id");
    }
}