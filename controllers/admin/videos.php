<?php

require_once __DIR__ . '/../base/admin/list.php';
require_once __DIR__ . '/../../views/admin/videos.php';
require_once __DIR__ . '/../../models/course.php';

class AdminListVideosView extends AdminListBaseController
{
    public function get_view()
    {
        return AdminVideosView::class;
    }

    public function get_data()
    {
        $user = Request::getUser();
        $user_id = $user->id;
        if ($user->type == 'superuser') {
            $postfix_sql = "users.id IS NOT NULL";
        } else {
            $postfix_sql = "users.id = $user_id";
        }
        $search_fields = $this->get_search_fields();
        if (isset($_GET["search"])) {
            $search = mysqli_real_escape_string(Model::getConnection(), $_GET["search"]);
            $value = "'%$search%'";
            $keren = array();
            // array_push()
            foreach ($search_fields as $field) {
                array_push($keren, "$field LIKE $value");
            }

            $postfix_sql .= ' AND ' . join(" OR ", $keren);
        }
        $rv = $this->get_model()::fetch($postfix_sql, [], "INNER JOIN wrapper_courses ON wrapper_courses.id=wrapper INNER JOIN users ON wrapper_courses.mentor=users.id", "*, users.id AS user_id, courses.id AS id");
        return $rv;
    }

    public function get_model()
    {
        return Course::class;
    }

    public function get_search_fields()
    {
        return ['title',];
    }
}