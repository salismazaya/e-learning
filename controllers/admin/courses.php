<?php

require_once __DIR__ . '/../base/admin/list.php';
require_once __DIR__ . '/../../views/admin/courses.php';
require_once __DIR__ . '/../../helpers/request.php';
require_once __DIR__ . '/../../models/wrapper_courses.php';

class AdminCoursesListController extends AdminListBaseController
{
    public function get_view()
    {
        return AdminCoursesView::class;
    }

    public function get_data()
    {
        $user = Request::getUser();
        $user_id = $user->id;
        if ($user->type == 'superuser') {
            $postfix_sql = "id IS NOT NULL";
        } else {
            $postfix_sql = "mentor = $user_id";
        }
        $search_fields = $this->get_search_fields();
        if (isset($_GET["search"])) {
            $search = mysqli_real_escape_string(Model::getConnection(), $_GET["search"]);
            $value = "'%$search%'";
            $keren = array();
            // array_push()
            foreach ($search_fields as $field) {
                array_push($keren, "$field LIKE $value");
            }

            $postfix_sql .= " AND " . join(" OR ", $keren);
        }
        return $this->get_model()::fetch($postfix_sql);
    }

    public function get_model()
    {
        return WrapperCourse::class;
    }

    public function get_search_fields()
    {
        return ['wrapper_name', 'description'];
    }
}