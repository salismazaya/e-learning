<?php

require_once __DIR__ . '/base/base.php';
require_once __DIR__ . '/../views/course.php';
require_once __DIR__ . '/../models/course.php';
require_once __DIR__ . '/../models/wrapper_courses.php';

class CourseController extends BaseController {
    public function get() {
        if (isset($_GET['id'])) {
            $real_course_id = mysqli_real_escape_string(Model::getConnection(), $_GET['id']);
            $video = Course::fetch("id = $real_course_id");
            $wrapper_id = $video[0]->wrapper;
            $course = WrapperCourse::fetch("id = $wrapper_id")[0];
            $course_id = $course->id;
            $video = Course::fetch("wrapper = $course_id ORDER BY CASE WHEN id = $real_course_id THEN 0 ELSE 1 END, id DESC");
            $view = new CourseView($course, $video[0], $video);
        } else {
            $course_id = mysqli_real_escape_string(Model::getConnection(), $_GET['wrapper']);
            $video = Course::fetch("wrapper = $course_id");
            $wrapper_id = $video[0]->wrapper;
            $course = WrapperCourse::fetch("id = $wrapper_id")[0];
            $view = new CourseView($course, $video[0], $video);
        }
        return $view->display();
    }
}