<?php

class BaseController
{
    public function post()
    {
        return "REQUEST NOT SUPPORTED";
    }
    public function get()
    {
        return "REQUEST NOT SUPPORTED";
    }
    public function put()
    {
        return "REQUEST NOT SUPPORTED";
    }

    public function handle()
    {
        if (session_status() === PHP_SESSION_NONE) {
            session_start();
        }
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            return $this->get();
        } else if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            return $this->post();
        } else if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
            return $this->put();
        }
        session_destroy();
    }
}