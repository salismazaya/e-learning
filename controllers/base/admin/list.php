<?php

require_once __DIR__ . '/base.php';
require_once __DIR__ . '/../../../views/admin/users.php';
require_once __DIR__ . '/../../../helpers/request.php';
require_once __DIR__ . '/../../../models/user.php';
require_once __DIR__ . "/../../../helpers/flash.php";
require_once __DIR__ . "/../../../helpers/filesystem.php";


abstract class AdminListBaseController extends AdminBaseController
{
    abstract public function get_view();
    abstract public function get_data();
    abstract public function get_model();
    abstract public function get_search_fields();

    public function get_postfix_sql_for_fetch_one()
    {
        return "id = ? LIMIT 1";
    }

    public function get()
    {
        $data = $this->get_data();
        $view_class = $this->get_view();
        $model_class = $this->get_model();

        $view = new $view_class($data);

        // set value view
        if (isset($_GET['id'])) {
            $keren = (array) $model_class::fetch($this->get_postfix_sql_for_fetch_one(), [$_GET['id']])[0];
            $view->set_value($keren);
        }

        return $view->display();
    }

    public function post()
    {
        $detector = new League\MimeTypeDetection\FinfoMimeTypeDetector();

        foreach ($_FILES as $key => $value) {
            if ($value['tmp_name'] == "") {
                continue;
            }

            if (str_starts_with($key, "video")) {
                if (!str_starts_with($detector->detectMimeTypeFromFile($value['tmp_name']), "video")) {
                    add_message("error", "File $key is not video");
                    continue;
                }
            }

            if (str_starts_with($key, "image")) {
                if (!str_starts_with($detector->detectMimeTypeFromFile($value['tmp_name']), "image")) {
                    add_message("error", "File $key is not image");
                    continue;
                }
            }

            $oh = explode('.', $value['name']);
            $file_type = end($oh);
            $name = base64_encode(random_bytes(10));
            $_POST[$key] = "$name.$file_type";
            get_filesystem()->writeStream("$name.$file_type", fopen($value['tmp_name'], 'r+'));
        }
        if (isset($_GET['action']) && $_GET['action'] == 'add') {
            $view_class = $this->get_view();
            foreach ($view_class::get_fields() as $key => $value) {
                if (!isset($_POST[$key])) {
                    add_message("error", $key . " is required");
                }
            }

            try {
                $model_class = $this->get_model();
                $success = $model_class::insert($_POST);
                if ($success) {
                    add_message("success", "Succesfully");
                }
            } catch (Exception $e) {
                add_message("error", $e->getMessage());
            }
        } else if (isset($_GET['action']) && $_GET['action'] == 'delete') {
            try {
                $ids = explode(",", $_GET['ids']);
                $idss = array();
                foreach ($ids as $key => $value) {
                    $value = mysqli_real_escape_string(Model::getConnection(), $value);
                    array_push($idss, "id = $value", );
                }

                $postfixSql = join(" OR ", $idss);

                $model_class = $this->get_model();
                $success = $model_class::delete($postfixSql);
                if ($success) {
                    add_message("success", "Succesfully");
                }
                return "OK";
            } catch (Exception $e) {
                add_message("error", $e->getMessage());
            }
        } else {
            $id = mysqli_real_escape_string(Model::getConnection(), $_GET['id']);
            try {
                $model_class = $this->get_model();
                $success = $model_class::update($_POST, "id = $id");
                if ($success) {
                    add_message("success", "Succesfully");
                }
            } catch (Exception $e) {
                add_message("error", $e->getMessage());
            }
        }


        header("Refresh:0");
    }
}