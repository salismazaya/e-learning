<?php

require_once __DIR__ . '/../base.php';
require_once __DIR__ . '/../../../helpers/request.php';

class AdminBaseController extends BaseController
{
    public function handle()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        $user = Request::getUser();
        if ($user === null || $user->type == 'user') {
            return header('Location: /login.php');
        }
        return parent::handle();
    }
}