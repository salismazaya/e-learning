<?php

require_once __DIR__ . '/base/base.php';
require_once __DIR__ . '/../views/index.php';

class IndexController extends BaseController {
    public function get() {
        $view = new IndexView();
        return $view->display();
    }
}