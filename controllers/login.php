<?php

require_once 'base/base.php';
require_once '../views/login.php';
require_once __DIR__ . '/../models/user.php';

class LoginController extends BaseController
{
    public function get()
    {
        $view = new LoginView();
        return $view->display();
    }

    public function post()
    {
        $view = new LoginView();

        if (!isset($_POST['username']) || !isset($_POST['password'])) {
            $view->setErrorMessage('Kocak lu');
            return $view->display();
        }

        $users = User::fetch("username = '?' LIMIT 1", [$_POST['username']]);
        if (empty($users)) {
            $view->setErrorMessage('Username not exists');
            return $view->display();
        }

        $user = $users[0];
        if (!password_verify($_POST['password'], $user->password)) {
            $view->setErrorMessage('Password not match');
            return $view->display();
        }

        $_SESSION['user_id'] = $user->id;

        if ($user->type == 'superuser' || $user->type == 'mentor') {
            header("Location: /admin");
        } else {
            header("Location: /courses.php");
        }
    }
}