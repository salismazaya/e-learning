<?php

require_once 'base/base.php';
require_once '../views/register.php';
require_once __DIR__ . '/../models/user.php';

class RegisterController extends BaseController
{
    public function get()
    {
        $view = new RegisterView();
        return $view->display();
    }

    public function post()
    {
        $view = new RegisterView();

        if (!isset($_POST['username']) || !isset($_POST['password']) || !isset($_POST['name']) || !isset($_POST['confirmPassword'])) {
            $view->setErrorMessage('Kocak lu');
            return $view->display();
        }

        if ($_POST['password'] !== $_POST['confirmPassword']) {
            $view->setErrorMessage('Password and confirm password not same');
            return $view->display();
        }

        try {
            User::insert([
                'username' => $_POST['username'],
                'password' => password_hash($_POST['password'], PASSWORD_DEFAULT),
                'name' => $_POST['name'],
            ]);
            header("Location: /login.php");
        } catch (Exception $e) {
            $view->setErrorMessage($e->getMessage());
            return $view->display();
        }
    }
}