<?php

require_once __DIR__ . '/base/base.php';
require_once __DIR__ . '/../views/profile.php';
require_once __DIR__ . '/../models/user.php';

class ProfileController extends BaseController {
    public function get() {
        $view = new ProfileView(Request::getUser());
        return $view->display();
    }
}