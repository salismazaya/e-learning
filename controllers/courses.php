<?php

require_once __DIR__ . '/base/base.php';
require_once __DIR__ . '/../views/courses.php';
require_once __DIR__ . '/../models/wrapper_courses.php';

class CoursesController extends BaseController {
    public function get() {
        $courses = WrapperCourse::fetch();
        $view = new CoursesView($courses);
        return $view->display();
    }
}