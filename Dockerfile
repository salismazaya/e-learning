FROM php:8.3-apache

# Install PHP extensions and necessary packages
RUN docker-php-ext-install mysqli 

# Update package list and install wget
RUN apt update -y && apt install wget -y

# Download and install Node.js
RUN wget https://nodejs.org/dist/v20.14.0/node-v20.14.0-linux-x64.tar.xz
RUN mkdir -p /usr/local/node
RUN tar --strip-components=1 -xJf node-v20.14.0-linux-x64.tar.xz -C /usr/local/node

# Add Node.js and npm to PATH
ENV PATH="/usr/local/node/bin:${PATH}"

COPY package.json .
RUN npm install


# Clean up
RUN rm node-v20.14.0-linux-x64.tar.xz

RUN apt install libxml2-dev -y
RUN docker-php-ext-install simplexml 
