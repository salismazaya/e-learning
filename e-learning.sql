-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: database
-- Generation Time: Jul 02, 2024 at 07:17 AM
-- Server version: 8.0.37
-- PHP Version: 8.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `e-learning`
--

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int UNSIGNED NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` text,
  `video_id` varchar(100) NOT NULL,
  `wrapper` int UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `title`, `description`, `video_id`, `wrapper`) VALUES
(8, 'TUTORIAL MEMASANG WEBSITE UANG KAS', 'Memasang aplikasi uang kas di render.com secara gratis', 'HHw33vqMu6+UVg==.mp4', 3),
(9, 'TEST VIDEO', 'TEST', 'RnrpzVdZL7fbwg==.mp4', 3),
(10, 'TEST VIDEO 2', 'TEST VIDEO 2', 'aM4+wpy7ohkbnQ==.mp4', 3);

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int UNSIGNED NOT NULL,
  `photo_id` varchar(100) NOT NULL,
  `course` int UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int UNSIGNED NOT NULL,
  `username` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `type` enum('superuser','mentor','user') NOT NULL DEFAULT 'user',
  `password` varchar(255) NOT NULL,
  `profile_picture` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `name`, `type`, `password`, `profile_picture`) VALUES
(1, 'salis', 'Salis', 'mentor', '$2y$10$LCEbhnj.3tuJhwoJRc2Zs.tvneqPujOJ2B.2nhTOlqbP/wWHxsaWC', NULL),
(6, 'salism3', 'Salis', 'superuser', '$2y$10$LCEbhnj.3tuJhwoJRc2Zs.tvneqPujOJ2B.2nhTOlqbP/wWHxsaWC', NULL),
(7, 'kasep', 'Salis Kasep', 'mentor', '$2y$10$tAl2Gn0USJ1GdR7DrueAW.48IrD141jJqk4i2b4CIcspsnPptTpvq', NULL),
(8, 'rahmat', 'Rahmat', 'superuser', '$2y$10$yXMsMtHhPPToQZWusTukheO24LDSgNxLhpbaEnBD4dQMffgjqbK3K', NULL),
(10, 'sekeren', 'Firman Maulana', 'user', '$2y$10$xt31v5ShDT187ihqFPsmkOVTJOW1J.a/.2UJr0vtpfbzrBRzJBgta', NULL),
(11, 'firman', 'Firman Malana', 'user', '$2y$10$CJs5lx1v.45UxoBwRqC9v.D9RYRqfW7nDEyCLCq1w3fVj/NL2eQ.y', NULL),
(12, 'firdan', 'Firdan Maulana', 'user', '$2y$10$0NdoPPuqPzkSyJD7YWStwO0EBLzKqq3I2ww.45r8kcrID3kjuEfMu', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wrapper_courses`
--

CREATE TABLE `wrapper_courses` (
  `id` int UNSIGNED NOT NULL,
  `wrapper_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `mentor` int UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `wrapper_courses`
--

INSERT INTO `wrapper_courses` (`id`, `wrapper_name`, `image`, `mentor`) VALUES
(3, 'Salis Random Tutorial', 'xeQ3SiGKOPP/3Q==.png', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `video_id` (`video_id`),
  ADD KEY `keren2` (`wrapper`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `photo_id` (`photo_id`),
  ADD KEY `course` (`course`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `wrapper_courses`
--
ALTER TABLE `wrapper_courses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `keren` (`mentor`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `wrapper_courses`
--
ALTER TABLE `wrapper_courses`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `courses`
--
ALTER TABLE `courses`
  ADD CONSTRAINT `keren2` FOREIGN KEY (`wrapper`) REFERENCES `wrapper_courses` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `wrapper_courses`
--
ALTER TABLE `wrapper_courses`
  ADD CONSTRAINT `keren` FOREIGN KEY (`mentor`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
